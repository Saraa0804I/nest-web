import styled from 'styled-components';
import { ChevronRightIcon } from '../../assets';

export const RotatableChevronRightIcon = styled(ChevronRightIcon)`
    transform: ${({ clicked }) => (clicked ? `rotate(90deg)` : `0`)};
    transition: all 0.2s;
`;
