import { Fade } from '@material-ui/core';
import React, { FC } from 'react';
import styled, { css } from 'styled-components';
import { Border, LoadingDots, Text } from '../basic';
import {
    Center,
    InteractiveStyle,
    mapColorToBackground,
    Margin,
    Overlay,
    Padding,
    Queue,
} from '../layout';
import { ButtonType } from './button.d';

const RawButton: FC<ButtonType> = ({
    leftIcon,
    rightIcon,
    loading,
    queueSize,
    textType,
    textRole,
    children,
    iconPaddingSize,
    ...props
}) => {
    return (
        <Border {...props} animationDelay={0.3}>
            <Queue>
                <Center height="100%">
                    <Padding
                        size={[
                            1,
                            rightIcon || loading ? 3 : 5,
                            1,
                            leftIcon ? 3 : 5,
                        ]}
                    >
                        <Queue size={queueSize || 5}>
                            {leftIcon && (
                                <Overlay top={0} bottom={0} left={1}>
                                    <Center>{leftIcon}</Center>
                                </Overlay>
                            )}
                            <Text
                                nowrap={true}
                                type={textType || 'primary3'}
                                role={textRole}
                            >
                                {children}
                            </Text>
                        </Queue>
                    </Padding>
                </Center>
                {loading && (
                    <Margin size={iconPaddingSize || [0, 4, 0, 0]}>
                        <Fade in={loading}>
                            <Center>
                                <div style={{ height: '100%', width: '15px' }}>
                                    <Center>
                                        <LoadingDots />
                                    </Center>
                                </div>
                            </Center>
                        </Fade>
                    </Margin>
                )}
                {!loading && rightIcon && (
                    <Margin size={iconPaddingSize || [0, 4, 0, 0]}>
                        <Center>
                            <div style={{ height: '100%', width: '15px' }}>
                                <Center>{rightIcon}</Center>
                            </div>
                        </Center>
                    </Margin>
                )}
            </Queue>
        </Border>
    );
};

const DisabledStyleButton = css`
    opacity: ${({ disabled }: any) => (disabled ? '0.5' : 1)};
    pointer-events: ${({ disabled }: any) => (disabled ? 'none' : 'all')};
`;

export const RawStyledButton = styled(RawButton)`
    ${({ loading }) => !loading && InteractiveStyle};
    ${DisabledStyleButton};
    pointer-events: ${({ loading }) => loading && 'none'};
    :hover {
        background-color: ${({ loading, backgroundHoverColor }) =>
            !loading && mapColorToBackground(backgroundHoverColor)};
    }
`;

export const StyledButton = (props) => {
    const { textRole } = props;
    return (
        <RawStyledButton
            {...props}
            borderSize={[2]}
            radius="xlarge"
            textRole={textRole || 'light'}
        />
    );
};

export const Button: FC<ButtonType> = (props) => {
    const { type, role } = props;
    const isButtonSecondary =
        role === 'secondary' ? 'purple.main' : 'blue.main';
    const isButtonHoverSecondary =
        role === 'secondary' ? 'purple.hover' : 'blue.hover';
    const isButtonBorderSecondary =
        role === 'secondary' ? 'purple.light' : 'blue.light';

    switch (type) {
        default:
            return (
                <StyledButton
                    {...props}
                    color={isButtonSecondary}
                    textRole={role || 'primary'}
                    paddingSize={[2, 5]}
                    backgroundColor="transparent.main"
                    backgroundHoverColor={isButtonBorderSecondary}
                />
            );
        case 'primary':
            return (
                <StyledButton
                    {...props}
                    color={isButtonSecondary}
                    paddingSize={[2, 5]}
                    backgroundColor={isButtonSecondary}
                    backgroundHoverColor={isButtonHoverSecondary}
                />
            );
        case 'small':
            return (
                <StyledButton
                    {...props}
                    color={isButtonSecondary}
                    textRole={role || 'primary'}
                    textType="primary6"
                    paddingSize={[0, 4]}
                    backgroundColor="transparent.main"
                    backgroundHoverColor={isButtonBorderSecondary}
                />
            );
        case 'error':
            return (
                <StyledButton
                    {...props}
                    color="red.main"
                    textRole="error"
                    paddingSize={[2, 5]}
                    backgroundHoverColor="red.light"
                />
            );
    }
};
