export interface ButtonType {
    type?: 'primary' | 'secondary' | 'small' | 'error';
    role?: 'primary' | 'secondary' | 'error';

    rightIcon?: React.ReactNode;
    leftIcon?: React.ReactNode;
    onClick?: () => any;
    loading?: boolean;
    disabled?: boolean;
    iconPaddingSize?: number | number[];
}
