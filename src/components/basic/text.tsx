import styled, { css } from 'styled-components';
import { colors, FontStyle } from '../../theme';
import { ColorType, InteractiveStyle } from '../layout';
import _ from 'lodash';

const mapRoleToText = {
    default: colors.gray[90],
    primary: colors.blue.main,
    secondary: colors.purple.main,
    tertiary: colors.gray[50],
    success: colors.green.main,
    alert: colors.orange.main,
    error: colors.red.main,
    light: colors.white.main,
    dark: colors.black.main,
};

export type TextRoleType = keyof typeof mapRoleToText;
export type TextTypeStyle = keyof typeof FontStyle;

type TextType = {
    role?: TextRoleType;
    color?: ColorType;
    type?: TextTypeStyle;
    nowrap?: boolean;
    underlined?: boolean;
    alignment?: 'left' | 'right' | 'center';
    interactive?: boolean;
    onClick?: any;
};

const mapColorToText = (color: ColorType) => {
    return _.get(colors, color || 'gray.90');
};

export const mapRoleToTextColor = (
    role: keyof typeof mapRoleToText | undefined
) => {
    return mapRoleToText[role || 'default'];
};

export const TextStyle = css<TextType>`
    font-family: 'Poppins', sans-serif;
    text-decoration: ${({ underlined }) => underlined && 'underline'};
    ${({ type }) => {
        return FontStyle[type || 'primary4'] || FontStyle.default;
    }}
    text-overflow: ${({ nowrap }) => nowrap && 'ellipsis'};
    overflow: ${({ nowrap }) => nowrap && 'hidden'};
    white-space: ${({ nowrap }) => nowrap && 'nowrap'};
    text-align: ${({ alignment }) => alignment || 'left'};
    cursor: ${({ onClick }) => onClick && 'pointer'};
`;

export const Text = styled.span<TextType>`
    ${TextStyle}
    color: ${({ role, color }) =>
        color
            ? mapColorToText(color)
            : mapRoleToTextColor(role || 'default')} !important;
    ${({ interactive }) => interactive && InteractiveStyle}
`;
