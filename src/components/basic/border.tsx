import _ from 'lodash';
import styled, { css } from 'styled-components';
import { colors } from '../../theme';
import { BackgroundStyle, DimensionStyle, InteractiveStyle } from '../layout';
import { BackgroundType, ColorType, DimensionType } from '../layout/types';

const mapRoleToBorder = {
    primary: colors.blue.light,
    secondary: colors.purple.light,
    tertiary: colors.aqua.main,
    success: colors.green.main,
    alert: colors.orange.main,
    error: colors.red.main,
    light: colors.white.main,
    dark: colors.black.main,
    transparent: colors.transparent.main,
};
export type BorderRoleType = keyof typeof mapRoleToBorder;
interface BorderType {
    role?: BorderRoleType;
    radius?: keyof typeof mapBorderRadius;
    color?: ColorType;
    selected?: boolean;
    borderSize?: number[];
    animationDelay?: string | number;
    overflow?: 'auto' | 'hidden' | 'scroll' | 'visible';
    interactive?: boolean;
    loading?: boolean;
    disabled?: boolean;
}

const mapBorderRadius = {
    circle: '50%',
    small: '3px',
    medium: '5px',
    large: '8px',
    xlarge: '100px',
};

export const mapRoleToBorderColor = (role: BorderRoleType) => {
    return mapRoleToBorder[role];
};

export const mapColorToBorder = (color: ColorType) => {
    return _.get(colors, color || 'gray.90');
};

const mapBorderRadiusByType = (
    type: keyof typeof mapBorderRadius | undefined
) => _.get(mapBorderRadius, `${type || '0px'}`);

const HoverableStyle = css<BorderType>`
    :hover {
        border-color: ${_.get(mapColorToBorder, 'secondary.main')};
    }
`;

const DisabledStyle = css<BorderType>`
    opacity: 0.1;
    pointer-events: none;
`;

export const BorderStyle = css<BorderType>`
    border-radius: ${({ radius }) => mapBorderRadiusByType(radius)};
    border-width: ${({ borderSize }) =>
        _.flow((borderSize) =>
            _.map(borderSize, (value) => {
                return `${value}px`;
            }).join(' ')
        )(borderSize)};
    border-color: ${({ role, color }) =>
        color
            ? mapColorToBorder(color)
            : mapRoleToBorderColor(role || 'transparent')};

    border-style: solid;
    position: relative;
    overflow: ${({ overflow }) => overflow};
`;

export const Border = styled.div<BorderType & DimensionType & BackgroundType>`
    transition: all ${({ animationDelay }) => animationDelay || '0.0'}s;
    ${({ interactive }) => interactive && HoverableStyle};
    ${({ onClick, loading }) => !loading && onClick && InteractiveStyle};
    ${BorderStyle};
    ${DimensionStyle};
    ${BackgroundStyle};
    ${({ disabled }) => disabled && DisabledStyle}
`;
