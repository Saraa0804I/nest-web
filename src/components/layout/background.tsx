import { css } from 'styled-components';
import { colors } from '../../theme';
import _ from 'lodash';
import { InteractiveStyle } from './styles';
import {
    ColorType,
    BackgroundRoleType,
    BackgroundType,
    HoverRoleType,
} from '../layout/types';

const mapRoleToBackground = {
    primary: colors.blue.main,
    secondary: colors.purple.main,
    tertiary: colors.gray[10],
    success: colors.green.main,
    alert: colors.orange.main,
    error: colors.red.main,
    light: colors.white.main,
    dark: colors.black.main,
    transparent: colors.transparent.main,
};
const mapRoleToHover = {
    primary: colors.blue.hover,
    secondary: colors.purple.hover,
    success: colors.green.hover,
    alert: colors.orange.hover,
    error: colors.red.hover,
};

export const mapColorToBackground = (color: ColorType) => {
    return _.get(colors, color || 'gray.90');
};

export const mapRoleToBackgroundColor = (
    role: BackgroundRoleType | undefined
) => {
    return _.get(mapRoleToBackground, `${role || 'transparent'}`);
};

export const mapRoleToHoverColor = (role: HoverRoleType) => {
    return _.get(mapRoleToHover, `${role}`);
};

const HoverableStyle = css<BackgroundType>`
    &:hover {
        background-color: ${({ hoverRole }) =>
            hoverRole &&
            `${mapRoleToHoverColor(hoverRole)}; ${InteractiveStyle}`};
    }
`;

export const BackgroundStyle = css`
    background-color: ${({ backgroundRole, backgroundColor }: BackgroundType) =>
        backgroundColor
            ? mapColorToBackground(backgroundColor)
            : mapRoleToBackgroundColor(backgroundRole)};
    ${HoverableStyle}
`;
