import { ReactNode } from 'react';

export type GridType = {
    space?: number;
    columns?: number | number[];
    align?: 'flex-end' | 'flex-start' | 'center';
    columnTemplate?: any;
};
export type StackType = {
    size?: number;
    alignVertical?:
        | ''
        | 'stretch'
        | 'center'
        | 'start'
        | 'end'
        | 'flex-end'
        | 'flex-start';
};
export type QueueType = {
    alignItems?: string;
    justifyContent?:
        | 'center'
        | 'start'
        | 'end'
        | 'flex-start'
        | 'flex-end'
        | 'left'
        | 'right'
        | 'space-between'
        | 'space-around'
        | 'space-evenly'
        | 'stretch';
    alignVertical?:
        | ''
        | 'stretch'
        | 'center'
        | 'start'
        | 'end'
        | 'flex-end'
        | 'flex-start';
    size?: number | number[];
};
export type DimensionType = {
    width?: string | number;
    height?: string | number;
    maxHeight?: string | number;
    maxWidth?: string | number;
    theme?: any;
    reverse?: boolean;
};
export type PaddingType = {
    size?: number | number[];
};
export type OverlayType = {
    visible?: boolean;
    relative?: boolean;
    zIndex?: number;
    top?: number | string;
    left?: number | string;
    right?: number | string;
    bottom?: number | string;
};
export type BackgroundRoleType =
    | 'primary'
    | 'secondary'
    | 'tertiary'
    | 'success'
    | 'alert'
    | 'error'
    | 'transparent'
    | 'dark'
    | 'light';
export type HoverRoleType =
    | 'primary'
    | 'secondary'
    | 'success'
    | 'alert'
    | 'error';
export type BackgroundType = {
    background?: boolean;
    backgroundRole?: BackgroundRoleType;
    backgroundColor?: ColorType;
    hoverRole?: HoverRoleType;
};
export type ContainerType = {
    theme?: any;
};
export type MarginType = {
    theme?: any;
    size: number | number[];
};

export type BoxType = {
    props?: any;
    image?: string | ReactNode;
    width?: string | number;
    ratio?: any;
    theme?: number;
};
export type CircleType = {
    theme?: any;
    size?: number;
};
export type ShadowType = {
    interactive?: boolean;
};
export type SpacerType = {
    size?: number;
    grow?: number;
    alignment?: 'right' | 'center' | 'left';
};
export type ColorType =
    | 'white.main'
    | 'black.main'
    | 'transparent'
    | 'gray.0'
    | 'gray.10'
    | 'gray.20'
    | 'gray.30'
    | 'gray.40'
    | 'gray.50'
    | 'gray.60'
    | 'gray.70'
    | 'gray.80'
    | 'gray.90'
    | 'blue.light'
    | 'blue.dark'
    | 'blue.main'
    | 'blue.hover'
    | 'purple.light'
    | 'purple.dark'
    | 'purple.main'
    | 'purple.hover'
    | 'aqua.light'
    | 'aqua.dark'
    | 'aqua.main'
    | 'aqua.hover'
    | 'green.light'
    | 'green.dark'
    | 'green.main'
    | 'green.hover'
    | 'orange.light'
    | 'orange.dark'
    | 'orange.main'
    | 'orange.hover'
    | 'red.light'
    | 'red.dark'
    | 'red.main'
    | 'red.hover';
